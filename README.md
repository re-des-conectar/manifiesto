# Re-Des(Conectar)

¡Acá estación [Okapia](https://okapia.land/)!

**Esta es una transmisión abierta**

Navegamos el horizonte de sucesos.

La transición es eminente.

---

Inte(i)rrumpir la red.

**Manifiesto abierto para Re-Des(Conectar)**

Recuperar lo que dejamos atrás.

Descubrir lo nuevo. Conectar con lo imposible.

---

[Archipiélago](https://archipielago.org/) orgánico.

**Hábitat de aprendizaje y creación conjunta**

Mil germinaciones de autonomías transitorias.

Cinco retos de tejido conegtivo:

   - Agua
   - Alimentos
   - Cobijo
   - Energía
   - Comunicación
---

Sin dependencia. Sin delegar.

**Asumir responsabilidad**

Autonomías complementarias.

Acciones recíprocas.

---

Hongos y algas.

**Simbiosis creativa**

Antropología transespecie.

Cibernética monstruosa.

---

¿Te interesa?

**Esto no es un juego ;)**

Realidades alternativas. Ucronía continua.

Aquí, pero en múltiples tiempos.

---

*Volcado de memoria...*

**Ruido que interrumpe la cotidianidad**

Glitch de lo habitual

Sigue el nexo [Plural](https://t.me/plural_bot).

---

Re-inicio decreciente.

Herramientas convivenciales.

**Referencia 1.1 Ivan Illich**

---

Nos encontramos limitados por dispositivos.

El lenguaje nos encierra en lo posible.

**Referencia 1.2 Giorgio Agamben**

---

Holobiontes que navegamos en la vida.

Y a la vez somos el mar de la vida.

**Referencia 1.3 Lynn Margulis**

---

Poner en paréntesis nuestra humanidad.

Asumir un pensamiento tentacular.

**Referencia 1.4 Donna Haraway**

---

Todo lo que la lluvia toca debe ser verde.

Nuestro hábitat necesita ser mimético.

**Referencia 1.5 Hundertwasser**

---

¿Te interesa?

**Eres ya parte de la transición**



`Git init_`



Damos inicio al repositorio.

Este es nuestro re-inicio.



`Git add .`

`Status: Nuevo documento_`



**Manifiesto Abierto para RE-DES(CONECTAR) V.0**



Tiempo de uso: indeterminado.

Licencia: sin dominio.



`Git commit -m “Primer deploy a la red distribuida”_`



Publicando el hash [Ápicca](https://apicca.com) de protocolo común.

Información abierta.



`Git Push_`


¡Ahora estamos en línea!